cd $WD 
echo "Downloading the genome"
mkdir -p res/genome

cd $WD/res/genome
wget -O ecoli.fasta.gz ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/005/845/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.fna.gz
gunzip -k ecoli.fasta.gz

cd $WD
echo " Creating directories"
mkdir -p log/cutadapt
mkdir -p out/cutadapt
mkdir -p out/fastqc
mkdir -p out/multiqc
mkdir -p out/multiqc/multiqc_data
mkdir -p out/star
mkdir -p res/genome/star_index

cd $WD
echo "Running FastQC..."
fastqc -o out/fastqc data/*.fastq.gz

cd $WD
echo "Running STAR index..."
mkdir -p res/genome/star_index
STAR --runThreadN 4 --runMode genomeGenerate --genomeDir res/genome/star_index/ --genomeFastaFiles res/genome/ecoli.fasta --genomeSAindexNbases 9

cd $WD
for sampleid in $(ls data/*.fastq.gz | cut -d"_" -f1 | sed 's:data/::' | sort | uniq)
do
    bash scripts/analyse_sample.sh $sampleid
done

cd $WD
echo "Creating a report..."
multiqc -o out/multiqc $WD

echo "Done"
